@file:Suppress("UnstableApiUsage")

import mko575.Utils

/* For an exemple file see https://plugins.gradle.org/docs/publish-plugin */

plugins {
    id("project-configuration.IDE")

    `kotlin-dsl` // id("org.gradle.kotlin.kotlin-dsl") version "4.3.0"

    // Apply the Java Gradle plugin development plugin to add support for developing Gradle plugins
    `java-gradle-plugin`

    // Apply the Kotlin JVM plugin to add support for Kotlin.
    // Inserted by the Gradle 'init' task: replaces the kotlin-dsl plugin call ?
    // alias(libs.plugins.jvm)

    // The 2 previous plugins should be called buy the publish plugin
    id("com.gradle.plugin-publish") version "1.2.1"
    // For how to publish to the Gradle Plugin Portal see:
    //   https://docs.gradle.org/current/userguide/publishing_gradle_plugins.html#publishing_portal
}

kotlin {
    // jvmToolchain(17)
}

repositories {
    mavenCentral()
}

/**
 * Dependencies declarations
 */

dependencies {
    // implementation(gradleApi())
    api(kotlin("gradle-plugin"))
}

/**
 * Plugins configuration
 */

val pluginOwnerId = Utils.getPluginInfo("PLUGIN_OWNER_ID")
val pluginName = Utils.getPluginInfo("PLUGIN_NAME")
val pluginPackage = Utils.fromSubProjOrRootProj(project, "Package")
val pluginVersion = Utils.getPluginInfo("PLUGIN_VERSION")

val lcPluginName: String = pluginName.lowercase()
val pluginIdSeed: String =
        "${pluginOwnerId}.${lcPluginName}"
                .replace(("[\\s_]+").toRegex(),"-")
                .replace(("[^\\w\\d.-]+").toRegex(), "")
                .replace(("\\.+").toRegex(), ".")

logger.quiet(
        "Configuring build for plugin '${pluginName}' (${pluginIdSeed} v${pluginVersion})" +
                " of '${pluginOwnerId}' in package '${pluginPackage}'"
)

version = pluginVersion
group = pluginOwnerId

gradlePlugin {
    website.set("https://gitlab.com/mko-utilities/gradle-plugins/zola")
    vcsUrl.set("https://gitlab.com/mko-utilities/gradle-plugins/zola.git")
    plugins {
        create("${lcPluginName}Base") {
            id = "${pluginIdSeed}-base"
            displayName = "Gradle Base Plugin for Zola"
            description = "Provides tasks and other Gradle artifacts definitions for Zola Gradle plugin"
            tags.set(listOf("Zola", "SSG", "wrapper", "base plugin"))
            implementationClass = "${pluginPackage}.${pluginName}BasePlugin"
            // version = "${version}"
        }
        create("zolaPlugin") {
            id = "${pluginIdSeed}"
            displayName = "Gradle Plugin for Zola"
            description = "Build Zola static sites with Gradle!"
            tags.set(listOf("Zola", "SSG", "wrapper"))
            implementationClass = "${pluginPackage}.${pluginName}Plugin"
            // version = "${version}"
        }
    }
}

/**
 * TESTING
 */

tasks.validatePlugins {
    enableStricterValidation = true
}

val currentGradleVersion: String = GradleVersion.current().version
val additionalGradleVersions = listOf<String>() // listOf("7.6.3")
val testGradleVersion = "testGradleVersion"
val displayNameSuffix = "displayNameSuffix"
testing {
    suites {
        named<JvmTestSuite>("test") {
            useJUnitJupiter()
            dependencies {
                implementation(gradleTestKit())
                implementation("org.assertj:assertj-core:3.24.2")
                implementation("org.junit.jupiter:junit-jupiter-api:5.10.1")
                implementation("io.mockk:mockk-jvm:1.13.8")
                runtimeOnly("org.junit.jupiter:junit-jupiter-engine")
            }
            targets {
                named("test") {
                    testTask {
                        systemProperties(testGradleVersion to currentGradleVersion, displayNameSuffix to "")
                    }
                }
                additionalGradleVersions.forEach { version ->
                    create("testWithGradle${version.replace(Regex("\\W"), "_")}") {
                        testTask {
                            systemProperties(testGradleVersion to version, displayNameSuffix to " - Gradle $version")
                            mustRunAfter(tasks.test)
                        }
                    }
                }
            }
        }
    }
}

/**
 * PUBLISHING
 */

val gradleUserHome: File = gradle.gradleUserHomeDir
val mkoRegistryDeployToken: String by project
publishing {
    repositories {
        maven {
            name = "Local"
            url = uri("${gradleUserHome}/local-plugin-repository")
        }
        maven {
            name = "GitLab"
            // url = uri("https://gitlab.com/api/v4/projects/46224752/packages/maven")
            url = uri("https://gitlab.com/api/v4/projects/mko-registry/java-packages/packages/maven")
            credentials(HttpHeaderCredentials::class) {
                name = "Deploy-Token"
                value = mkoRegistryDeployToken
            }
            authentication {
                create<HttpHeaderAuthentication>("header")
            }
        }
    }
}

val gradleWrapperVersion: String by project
tasks.wrapper {
    gradleVersion = gradleWrapperVersion
}
