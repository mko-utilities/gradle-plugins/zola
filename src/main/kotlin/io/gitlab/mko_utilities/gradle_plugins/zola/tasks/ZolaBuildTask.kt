package io.gitlab.mko_utilities.gradle_plugins.zola.tasks

import org.gradle.api.file.DirectoryProperty
import org.gradle.api.provider.Property
import org.gradle.api.tasks.*
import org.gradle.api.tasks.options.Option

@CacheableTask
abstract class ZolaBuildTask : ZolaSubcommandTask() {

    /********************************************************************/
    /**  TASK INPUT/OUTPUT PROPERTIES                                  **/
    /********************************************************************/

    /**
     * The base URL to use for generation.
     */
    @get:Input
    @get:Optional
    @get:Option(
        option="base-url",
        description = "Force the base URL to be the provided value (defaults" +
                " to the one in the Zola config file)."
    )
    abstract val baseURL: Property<String>

    /**
     * Whether to include the drafts.
     */
    @get:Input
    @get:Optional
    @get:Option(
        option="drafts",
        description = "Include drafts when generating the site."
    )
    abstract val drafts: Property<Boolean>

    /**
     * Directory where to output generated website files.
     */
    @get:OutputDirectory
    @get:Option(
        option="output-dir",
        description = "Outputs the generated site in the given path."
    )
    abstract val outputDirectory: DirectoryProperty // = layout.buildDirectory.dir(PUBLISH_DIRECTORY).get().asFile

    /********************************************************************/
    /**  TASK ACTION                                                   **/
    /********************************************************************/

    @TaskAction
    override fun run() {
        execZolaSubcommand("build", "")
    }

    protected open fun execZolaSubcommand(subcommand: String, parameters: String) {
        outputDirectory.get().asFile.deleteRecursively()
        outputDirectory.get().asFile.mkdirs()

        val definedOutputDirParam =
            if (outputDirectory.isPresent)
                "--output-dir ${outputDirectory.get().asFile.absolutePath}"
            else ""

        val commandToBeExecuted =
            listOf(
                subcommand,
                "--force",
                baseURL.map{ "--base-url $it" }.getOrElse(""),
                drafts.map{ if (it) "--drafts" else "" }.getOrElse(""),
                definedOutputDirParam,
                parameters
            )
                .filterNot(String::isEmpty)
                .joinToString(" ")

        super.execZolaSubcommand(commandToBeExecuted)
    }
}
