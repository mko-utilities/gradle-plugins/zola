package io.gitlab.mko_utilities.gradle_plugins.zola.tasks

import org.gradle.api.DefaultTask
import org.gradle.api.file.ArchiveOperations
import org.gradle.api.file.FileSystemOperations
import org.gradle.api.file.RegularFileProperty
import org.gradle.api.provider.Property
import org.gradle.api.resources.TextResource
import org.gradle.api.tasks.CacheableTask
import org.gradle.api.tasks.Input
import org.gradle.api.tasks.OutputFile
import org.gradle.api.tasks.TaskAction
import org.jetbrains.kotlin.gradle.internal.ensureParentDirsCreated
import java.io.*
import java.net.URI
import java.nio.file.Files
import java.util.zip.ZipException
import javax.inject.Inject
import kotlin.io.path.ExperimentalPathApi
import kotlin.io.path.deleteRecursively

/**
 * Downloads the archive identified by {@link #archiveURI}, and extracts the file at {@link filePathInArchive}
 * in the archive into {@link extractedLocalFile}.
 */
@CacheableTask
abstract class RetrieveFileFromRemoteArchiveTask : RetrieveFileFromLocalArchiveTask() {

    // TEMPORARY HACK
    private val localArchive: File? =
        // File(System.getProperty("user.home") + "/Downloads/zola-v0.18.0-x86_64-unknown-linux-gnu.tar.gz")
        null

    /********************************************************************/
    /**  TASK INPUT/OUTPUT PROPERTIES                                  **/
    /********************************************************************/

    /**
     * The URL of the archive to retrieve.
     */
    @get:Input
    abstract val archiveURI: Property<String>

    /********************************************************************/
    /**  TASK ACTION                                                   **/
    /********************************************************************/

    /**
     * Action executed by the task.
     */
    @OptIn(ExperimentalPathApi::class)
    @TaskAction
    fun retrieveRemotely() {
        val tmpArchiveDir = Files.createTempDirectory("zola_archive_download")
        val tmpArchiveFile = downloadArchive(tmpArchiveDir.toFile())
        extractFileFrom(tmpArchiveFile)
        tmpArchiveDir.deleteRecursively()
    }

    /**
     * Downloads the archive identified by {@link #archiveURI} into the provider directory.
     * @param intoDir: the directory where to download the archive.
     * @return the {@link File} containing the local copy of the archive.
     */
    private fun downloadArchive(intoDir: File): File {
        val archiveURL = if (localArchive != null && localArchive.isFile) localArchive.toURI().toURL() else URI(archiveURI.get()).toURL()
        val archiveLocalCopy: File = File(intoDir, archiveURL.file)
        logger.info("Downloading archive from $archiveURL into $archiveLocalCopy")
        archiveLocalCopy.ensureParentDirsCreated()
        archiveURL.openStream().use { input ->
            FileOutputStream(archiveLocalCopy).use { output ->
                input.copyTo(output)
            }
        }
        return archiveLocalCopy
    }

}