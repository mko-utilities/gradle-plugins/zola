package io.gitlab.mko_utilities.gradle_plugins.zola.tasks

import org.gradle.api.DefaultTask
import org.gradle.api.file.ArchiveOperations
import org.gradle.api.file.FileSystemOperations
import org.gradle.api.file.RegularFileProperty
import org.gradle.api.provider.Property
import org.gradle.api.tasks.CacheableTask
import org.gradle.api.tasks.Input
import org.gradle.api.tasks.OutputFile
import org.gradle.api.tasks.TaskAction
import java.io.*
import java.nio.file.Files
import java.nio.file.Path
import java.util.zip.ZipException
import javax.inject.Inject
import kotlin.io.path.ExperimentalPathApi
import kotlin.io.path.deleteRecursively

/**
 * Downloads the archive identified by {@link #archiveURI}, and extracts the file at {@link filePathInArchive}
 * in the archive into {@link extractedLocalFile}.
 */
@CacheableTask
abstract class RetrieveFileFromLocalArchiveTask : DefaultTask() {

    /********************************************************************/
    /**  TASK INPUT/OUTPUT PROPERTIES                                  **/
    /********************************************************************/

    /**
     * The URL of the archive to retrieve.
     */
    @get:Input
    abstract val archivePath: Property<String>

    /**
     * The path to the executable inside the archive.
     */
    @get:Input
    abstract val filePathInArchive: Property<String>

    /**
     * The File into which to copy the retrieved Zola executable.
     */
    @get:OutputFile
    abstract val extractedLocalFile: RegularFileProperty

    /********************************************************************/
    /**  TASK INTERNAL FIELDS                                          **/
    /********************************************************************/

    @get:Inject
    protected abstract val fsOps: FileSystemOperations

    @get:Inject
    protected abstract val archOps: ArchiveOperations

    /********************************************************************/
    /**  TASK ACTION                                                   **/
    /********************************************************************/

    /**
     * Action executed by the task.
     */
    @TaskAction
    fun retrieveLocally() {
        extractFileFrom(File(archivePath.get()))
    }

    @OptIn(ExperimentalPathApi::class)
    protected fun extractFileFrom(archiveFile: File) {
        val tmpArchiveDir: Path = Files.createTempDirectory("zola_archive_extraction")
        when (archiveFile.extension) {
            "zip" -> fsOps.copy { from(archOps.zipTree(archiveFile)); into(tmpArchiveDir) }
            "tgz", "tar", "gz" -> fsOps.copy { from(archOps.tarTree(archiveFile)); into(tmpArchiveDir) }
            else -> throw ZipException("Unsupported extension for archive ${archiveFile.name}.")
        }
        // get() then asFile, and not executableFile.map(RegularFile::getAsFile).get(), because Gradle
        // does not like mapping on output properties.
        val copyTargetFile: File = extractedLocalFile.get().asFile
        File(tmpArchiveDir.toFile(), filePathInArchive.get())
            .copyTo(copyTargetFile, overwrite = true)
        tmpArchiveDir.deleteRecursively()
    }

}