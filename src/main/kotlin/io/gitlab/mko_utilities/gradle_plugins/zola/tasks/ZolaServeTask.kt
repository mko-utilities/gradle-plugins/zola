package io.gitlab.mko_utilities.gradle_plugins.zola.tasks

import org.gradle.api.provider.Property
import org.gradle.api.tasks.Input
import org.gradle.api.tasks.Optional
import org.gradle.api.tasks.TaskAction
import org.gradle.api.tasks.options.Option
import org.gradle.work.DisableCachingByDefault

@DisableCachingByDefault(because = "Serve task, needs to run every time")
abstract class ZolaServeTask : ZolaBuildTask() {

    /********************************************************************/
    /**  TASK INPUT/OUTPUT PROPERTIES                                  **/
    /********************************************************************/

    /**
     * The host on which to serve the website.
     */
    @get:Input
    @get:Optional
    @get:Option(
        option="interface",
        description = "The host on which to serve the website."
    )
    abstract val host: Property<String>

    /**
     * The port on which to serve the website.
     */
    @get:Input
    @get:Optional
    @get:Option(
        option="port",
        description = "The port on which to serve the website."
    )
    abstract val port: Property<String>

    /**
     * Whether to only rebuild the minimum on change (useful when working on a
     * specific page/section).
     */
    @get:Input
    @get:Optional
    @get:Option(
        option="fast",
        description = "Whether to only rebuild the minimum on change (useful" +
                " when working on a specific page/section)."
    )
    abstract val fast: Property<Boolean>

    /**
     * Whether to open site in the default browser.
     */
    @get:Input
    @get:Optional
    @get:Option(
        option="open",
        description = "Whether to open site in the default browser."
    )
    abstract val open: Property<Boolean>

    /********************************************************************/
    /**  TASK ACTION                                                   **/
    /********************************************************************/

    @TaskAction
    override fun run() {
        execZolaSubcommand("serve", "")
    }

    override fun execZolaSubcommand(subcommand: String, parameters: String) {
        val parametersToBeUsed =
            listOf(
                host.map{ "--interface $it" }.getOrElse(""),
                port.map{ "--port $it" }.getOrElse(""),
                fast.map{ if (it) "--fast" else "" }.getOrElse(""),
                open.map{ if (it) "--open" else "" }.getOrElse(""),
                parameters
            )
                .filterNot(String::isEmpty)
                .joinToString(" ")

        super.execZolaSubcommand(subcommand, parametersToBeUsed)
    }
}
