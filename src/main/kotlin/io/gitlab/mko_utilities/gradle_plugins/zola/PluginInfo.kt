package io.gitlab.mko_utilities.gradle_plugins.zola

object PluginInfo {
    const val PROJECT_NAME = "zola-gradle-plugin"
    const val PLUGIN_OWNER_ID = "io.gitlab.mko575"
    const val PLUGIN_GROUP = "io.gitlab.mko_utilities.gradle_plugins"
    const val PLUGIN_NAME = "Zola"
    val PLUGIN_ID = "$PLUGIN_OWNER_ID.${PLUGIN_NAME.lowercase()}"
    const val PLUGIN_VERSION = "0.1.1"
    val PLUGIN_EXTENSION_ID = "${PLUGIN_NAME.lowercase()}"
    const val PLUGIN_INFO_TASK_NAME = "zolaPluginInfo"
}