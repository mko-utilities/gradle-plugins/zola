package io.gitlab.mko_utilities.gradle_plugins.zola.tasks

import org.gradle.api.DefaultTask
import org.gradle.api.file.DirectoryProperty
import org.gradle.api.file.RegularFileProperty
import org.gradle.api.provider.Property
import org.gradle.api.tasks.*
import org.gradle.api.tasks.PathSensitive
import org.gradle.api.tasks.PathSensitivity.ABSOLUTE
import org.gradle.api.tasks.options.Option
import org.gradle.process.ExecOperations
import org.gradle.work.DisableCachingByDefault
import java.nio.file.Files
import java.nio.file.attribute.PosixFilePermissions
import javax.inject.Inject

@DisableCachingByDefault(because = "generic task with unpredictable outputs")
abstract class ExecuteConfigurableSubcommandTask : DefaultTask() {

    /********************************************************************/
    /**  TASK INPUT/OUTPUT PROPERTIES                                  **/
    /********************************************************************/

    /**
     * The executable to use to execute the command.
     */
    @get:InputFile
    @get:PathSensitive(ABSOLUTE)
    abstract val executableFile: RegularFileProperty

    /**
     * The working directory from which to execute the command.
     */
    @get:InputDirectory
    @get:PathSensitive(ABSOLUTE)
    @get:Option(option="working-dir", description = "The working directory from which to execute the command.")
    abstract val workingDirectory: DirectoryProperty

    /**
     * The command to execute.
     */
    @get:Input
    @get:Optional
    @get:Option(option="extra-arguments", description = "The command to execute.")
    abstract val extraArguments: Property<String>

    /********************************************************************/
    /**  TASK INTERNAL FIELDS                                          **/
    /********************************************************************/

    @get:Inject
    protected abstract val process: ExecOperations

    /********************************************************************/
    /**  TASK ACTION                                                   **/
    /********************************************************************/

    @TaskAction
    open fun run() {
        execSubcommand("")
    }

    /**
     * Execute the task with the command arguments, provided in {@code #command},
     * appended to the end of the command.
     * @param subcommand the subcommand to execute.
     */
    protected fun execSubcommand(subcommand: String) {
        val commandToBeExecuted =
            listOf(
                subcommand,
                extraArguments.getOrElse("")
            )
                .filterNot(String::isEmpty)
                .joinToString(" ")

        logger.info(
            "Will execute command '$commandToBeExecuted'"
                + " from working directory '${workingDirectory.get()}'"
                + " using executable '${executableFile.get()}'."
        )

        val execDir = workingDirectory.get().asFile
        execDir.mkdirs()
        val execFile = executableFile.get().asFile
        val originalPermissions = Files.getPosixFilePermissions(execFile.toPath())
        Files.setPosixFilePermissions(execFile.toPath(), PosixFilePermissions.fromString("r-xr-xr-x"))
        process.exec {
            workingDir = execDir
            executable = executableFile.get().asFile.absolutePath
            args = commandToBeExecuted.split(" ")
        }
        Files.setPosixFilePermissions(execFile.toPath(), originalPermissions)
    }
}
