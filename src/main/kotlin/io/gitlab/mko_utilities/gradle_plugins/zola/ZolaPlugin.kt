package io.gitlab.mko_utilities.gradle_plugins.zola

import io.gitlab.mko_utilities.gradle_plugins.zola.PluginInfo.PLUGIN_NAME
import io.gitlab.mko_utilities.gradle_plugins.zola.PluginInfo.PLUGIN_ID
import io.gitlab.mko_utilities.gradle_plugins.zola.PluginInfo.PLUGIN_INFO_TASK_NAME
import io.gitlab.mko_utilities.gradle_plugins.zola.PluginInfo.PLUGIN_VERSION
import io.gitlab.mko_utilities.gradle_plugins.zola.PluginInfo.PLUGIN_OWNER_ID
import io.gitlab.mko_utilities.gradle_plugins.zola.PluginInfo.PLUGIN_EXTENSION_ID
import io.gitlab.mko_utilities.gradle_plugins.zola.extensions.ZolaPluginExtension
import io.gitlab.mko_utilities.gradle_plugins.zola.tasks.*
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.logging.Logger
import org.gradle.api.logging.Logging
import org.gradle.api.DefaultTask
import org.gradle.api.artifacts.Configuration
import org.gradle.kotlin.dsl.dependencies
import org.gradle.kotlin.dsl.repositories
import java.io.File
import java.net.URI
import java.nio.file.Files
import java.nio.file.attribute.PosixFilePermissions
import java.util.concurrent.TimeUnit

class ZolaPlugin : Plugin<Project> {

    companion object {
        val LOGGER: Logger = Logging.getLogger(ZolaPlugin::class.java)
        const val RETRIEVE_EXECUTABLE_TASK_NAME: String = "zolaRetrieveExecutable"
        const val COMMAND_TASK_NAME: String = "zolaCommand"
        const val INIT_TASK_NAME: String = "zolaInit"
        const val BUILD_TASK_NAME: String = "zolaBuild"
        const val SERVE_TASK_NAME: String = "zolaServe"

        /**
         * Test if a file is an executable for Zola in a specific verion.
         * @param zolaExecFile the executable file to test. It is executed with the sole argument "--version".
         * @param expectedVersion the Zola version expected.
         * @return true iff {@code zolaExecFile} is a Zola executable in the {@code expectedVersion} version.
         */
        fun testZolaExecutableVersion(zolaExecFile: File, expectedVersion: String): Boolean {
            val originalPermissions = Files.getPosixFilePermissions(zolaExecFile.toPath())
            Files.setPosixFilePermissions(zolaExecFile.toPath(), PosixFilePermissions.fromString("r-xr-xr-x"))
            val processBuilder = ProcessBuilder(zolaExecFile.absolutePath, "--version")
            processBuilder.directory(Files.createTempDirectory("zolaExecTest").toFile())
            val process = processBuilder.start()
            process.waitFor(1, TimeUnit.SECONDS)
            Files.setPosixFilePermissions(zolaExecFile.toPath(), originalPermissions)
            val testStdOutLines = process.inputReader().lines()
            return testStdOutLines.anyMatch{ it.equals("zola $expectedVersion") }
//            val testStdOut = process.inputReader().lines().collect(Collectors.joining())
//            return testStdOut.contains(expectedVersion)
        }
    }

    override fun apply(project: Project) {
        with(project) {
            plugins.apply(ZolaBasePlugin::class.java)

            val pluginExtension =
                extensions.create(PLUGIN_EXTENSION_ID, ZolaPluginExtension::class.java, project)
            // pluginExtension.applyTo(project)

//            val zolaRepo = repositories.ivy {
//                    url = pluginExtension.ivyRepoURL.map{ URI(it) }.get()
//                    patternLayout {
//                        artifact(pluginExtension.ivyRepoPatternLayout.get())
//                    }
//                    metadataSources { artifact() }
//            }
//            repositories.add(zolaRepo)
//            repositories.exclusiveContent {
//                forRepositories(zolaRepo)
//                filter { includeGroup("getzola") }
//            }
            repositories {
                // val zolaRepo =
                ivy {
                    url = pluginExtension.ivyRepoURL.map{ URI(it) }.get()
                    patternLayout {
                        artifact(pluginExtension.ivyRepoPatternLayout.get())
                    }
                    metadataSources { artifact() }
                    content {
                        includeGroup("getzola")
                    }
                }
//                exclusiveContent {
//                    forRepositories(zolaRepo)
//                    filter { includeGroup("getzola") }
//                }
            }

            val zolaArchiveConf: Configuration = configurations.create("zolaArchiveConf")

            dependencies {
                zolaArchiveConf(pluginExtension.zolaArchiveArtifactId)
            }

            // Register plugin info task
            tasks.register(PLUGIN_INFO_TASK_NAME, DefaultTask::class.java) {
                group = PLUGIN_NAME
                description = "This tasks outputs information about the version" +
                        " and configuration of the $PLUGIN_NAME plugin."
                        
                doLast {
                    println(
                        "This is plugin '$PLUGIN_NAME' ($PLUGIN_ID v$PLUGIN_VERSION)" +
                                " of '$PLUGIN_OWNER_ID'" +
                                " in package '${this@ZolaPlugin::class.java.`package`.name}'")
                    println(
                        "$PLUGIN_NAME plugin is configured as follows:" +
                                pluginExtension
                                    .getConfigurationOptionsAsStringList{ n, v -> "   - $n = $v"}
                                    .joinToString(";\n", prefix = "\n", postfix = ".")
                    )
                }
            }

//            val zolaRetrieveExec = tasks.register(
//                RETRIEVE_EXECUTABLE_TASK_NAME, RetrieveFileFromRemoteArchiveTask::class.java
//            ) {
//                group = PLUGIN_NAME
//                description = "Retrieve the Zola executable from the web for the current OS (Windows, MacOS or Linux)."
//
//                archiveURI.set(pluginExtension.archiveURI)
//                filePathInArchive.set(pluginExtension.executablePathInArchive)
//                extractedLocalFile.set(pluginExtension.executableFile)
//
//                outputs.upToDateWhen {
//                    val execFile = extractedLocalFile.get().asFile
//                    val version = pluginExtension.version.get()
//                    execFile.isFile && testZolaExecutableVersion(execFile, version)
//                }
//            }

            val zolaRetrieveExec = tasks.register(
                RETRIEVE_EXECUTABLE_TASK_NAME, RetrieveFileFromLocalArchiveTask::class.java
            ) {
                group = PLUGIN_NAME
                description = "Retrieve the Zola executable from the web for the current OS (Windows, MacOS or Linux)."

                archivePath.set(zolaArchiveConf.asPath)
                filePathInArchive.set(pluginExtension.executablePathInArchive)
                extractedLocalFile.set(pluginExtension.executableFile)

                outputs.upToDateWhen {
                    val execFile = extractedLocalFile.get().asFile
                    val version = pluginExtension.version.get()
                    execFile.isFile && testZolaExecutableVersion(execFile, version)
                }
            }

            tasks.register(COMMAND_TASK_NAME, ZolaSubcommandTask::class.java) {
                group = PLUGIN_NAME
                description =
                    "Execute any Zola command (e.g. init, build, ...)." +
                            " The command executed can be overwritten with the --command parameter." +
                            " It defaults to \"help\""
                executableFile.set(pluginExtension.executableFile)
                workingDirectory.set(project.layout.projectDirectory)
                rootDirectory.set(pluginExtension.sourceDirectory)
                // Rely on the default Zola config file
                extraArguments.convention("help")
                dependsOn(zolaRetrieveExec)
            }

            tasks.register(INIT_TASK_NAME, ZolaSubcommandTask::class.java) {
                group = PLUGIN_NAME
                description =
                    "Initialize the Zola sources directory." +
                            " Beware of potential problems if the sources already exist."
                executableFile.set(pluginExtension.executableFile)
                workingDirectory.set(project.layout.projectDirectory)
                val parentSrcDir = pluginExtension.sourceDirectory.map{ File(it.asFile.parent) }
                val srcDirName = pluginExtension.sourceDirectory.get().asFile.name
                rootDirectory.set(parentSrcDir.get())
                // Rely on the default Zola config file
                extraArguments.convention("init $srcDirName")
                dependsOn(zolaRetrieveExec)
            }

            tasks.register(BUILD_TASK_NAME, ZolaBuildTask::class.java) {
                group = PLUGIN_NAME
                description = "Build Zola static site for publication."

                dependsOn(zolaRetrieveExec)

                executableFile.set(pluginExtension.executableFile)
                workingDirectory.set(project.layout.projectDirectory)
                rootDirectory.set(pluginExtension.sourceDirectory)
                outputDirectory.set(pluginExtension.publishDirectory)
            }

            tasks.register(SERVE_TASK_NAME, ZolaServeTask::class.java) {
                group = PLUGIN_NAME
                description = "Run server for development of Zola static site."

                dependsOn(zolaRetrieveExec)

                executableFile.set(pluginExtension.executableFile)
                workingDirectory.set(project.layout.projectDirectory)
                rootDirectory.set(pluginExtension.sourceDirectory)
                outputDirectory.set(pluginExtension.publishDirectory)
                host.set(pluginExtension.serveOnHost)
                port.set(pluginExtension.serveOnPort)
                fast.set(pluginExtension.serveFast)
                open.set(pluginExtension.autoOpenServedSite)
            }

        }
    }

}
