package io.gitlab.mko_utilities.gradle_plugins.zola.tasks

import org.gradle.api.file.DirectoryProperty
import org.gradle.api.file.RegularFileProperty
import org.gradle.api.tasks.*
import org.gradle.api.tasks.PathSensitivity.ABSOLUTE
import org.gradle.api.tasks.options.Option
import org.gradle.work.DisableCachingByDefault

@DisableCachingByDefault(because = "generic task with unpredictable outputs")
abstract class ZolaSubcommandTask : ExecuteConfigurableSubcommandTask() {

    /********************************************************************/
    /**  TASK INPUT/OUTPUT PROPERTIES                                  **/
    /********************************************************************/

    /**
     * The directory containing the website sources to process with Zola.
     */
    @get:InputDirectory
    @get:PathSensitive(ABSOLUTE)
    @get:Optional
    @get:Option(
        option="root",
        description = "The directory containing the website sources to process" +
                " with Zola."
    )
    abstract val rootDirectory: DirectoryProperty

    /**
     * The Zola config file to use in replacement to the 'config.toml' file in
     * the website sources directory.
     */
    @get:InputFile
    @get:PathSensitive(ABSOLUTE)
    @get:Optional
    @get:Option(
        option="config",
        description = "The Zola config file to use in replacement to the" +
                " 'config.toml' file in the website sources directory."
    )
    abstract val configFile: RegularFileProperty

    /********************************************************************/
    /**  TASK ACTION                                                   **/
    /********************************************************************/

    @TaskAction
    override fun run() {
        execZolaSubcommand("")
    }

    protected fun execZolaSubcommand(zolaSubcommand: String) {
        val commandToBeExecuted =
            listOf(
                rootDirectory.map{ "--root ${it.asFile.absolutePath}" }.getOrElse(""),
                configFile.map{ "--config ${it.asFile.absolutePath}" }.getOrElse(""),
                zolaSubcommand,
            )
                .filterNot(String::isEmpty)
                .joinToString(" ")

        super.execSubcommand(commandToBeExecuted)
    }
}
