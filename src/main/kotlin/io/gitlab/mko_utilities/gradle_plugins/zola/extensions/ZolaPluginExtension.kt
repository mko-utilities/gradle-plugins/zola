package io.gitlab.mko_utilities.gradle_plugins.zola.extensions

import org.apache.tools.ant.taskdefs.condition.Os
import org.apache.tools.ant.taskdefs.condition.Os.FAMILY_MAC
import org.apache.tools.ant.taskdefs.condition.Os.FAMILY_UNIX
import org.apache.tools.ant.taskdefs.condition.Os.FAMILY_WINDOWS
import org.gradle.api.Project
import org.gradle.api.file.Directory
import org.gradle.api.file.ProjectLayout
import org.gradle.api.file.RegularFile
import org.gradle.api.logging.Logger
import org.gradle.api.logging.Logging
import org.gradle.api.model.ObjectFactory
import org.gradle.api.provider.Property
import org.gradle.api.provider.Provider
import org.gradle.api.tasks.Input
import org.gradle.api.tasks.Internal
import java.io.File
import java.lang.Error
import java.net.URI
import java.net.URL
import java.text.MessageFormat

open class ZolaPluginExtension (
    /**
     * The project for which the plugin extension has been created.
     */
    project: Project
) {

    /**
     * Convenience property to manipulate the project's object factory.
     */
    private val objFact: ObjectFactory = project.objects

    /**
     * Convenience property to manipulate the project's object factory.
     */
    private val projectLayout: ProjectLayout = project.layout

    companion object Constants {
        private val LOGGER: Logger = Logging.getLogger(ZolaPluginExtension::class.java)
        const val ARCH = "x86_64"
        const val VERSION = "0.18.0"
        const val IVY_REPO_URL = "https://github.com/"
        const val IVY_REPO_PATTERN_LAYOUT = "/[organisation]/[module]/releases/download/v[revision]/[module]-v[revision]-[classifier].[ext]"
        const val WINDOWS_IVY_ARTIFACT_ID_PATTERN = "getzola:zola:{0}:{1}-pc-windows-msvc@zip"
        const val LINUX_IVY_ARTIFACT_ID_PATTERN = "getzola:zola:{0}:{1}-unknown-linux-gnu@tar.gz"
        const val MAC_IVY_ARTIFACT_ID_PATTERN = "getzola:zola:{0}:{1}-apple-darwin@tar.gz"
        const val DOWNLOAD_URL_PREFIX = "https://github.com/getzola/zola/releases/download"
        const val WINDOWS_DOWNLOAD_URL_PATTERN = "{0}/v{2}/zola-v{2}-{1}-pc-windows-msvc.zip"
        const val LINUX_DOWNLOAD_URL_PATTERN = "{0}/v{2}/zola-v{2}-{1}-unknown-linux-gnu.tar.gz"
        const val MAC_OS_DOWNLOAD_URL_PATTERN = "{0}/v{2}/zola-v{2}-{1}-apple-darwin.tar.gz"
        const val EXECUTABLE_PATH_IN_WINDOWS_ARCHIVE = "zola.exe"
        const val EXECUTABLE_PATH_IN_MAC_ARCHIVE = "zola"
        const val EXECUTABLE_PATH_IN_LINUX_ARCHIVE = "zola"
        const val BUILD_TOOLS_DIR_RELPATH = "buildTools"
        const val EXECUTABLE_DIR_RELPATH = "zola/bin"
        const val SOURCE_DIRECTORY_RELPATH = "src/site"
        const val PUBLISH_DIRECTORY_RELPATH = "zola/publish"
        const val SERVE_HOST = "127.0.0.1"
        const val SERVE_PORT = "1111"
        const val SERVE_FAST = false
        const val SERVE_OPEN = false
    }

    /**
     * Convenience method to create properties with an initial value.
     */
    private fun <T> newProperty(clazz: Class<T>, value: T): Property<T> {
        return objFact.property(clazz).also{ it.convention(value) }
    }

    /**
     * Convenience method to create properties with an initial value bound to another provider.
     */
    private fun <T> newProperty(clazz: Class<T>, valueProvider: Provider<T>): Property<T> {
        return objFact.property(clazz).also{ it.convention(valueProvider) }
    }

    /**
     * The OS to assume for Zola process executions.
     */
    @Input
    val osFamily: Property<String> =
        newProperty(
            String::class.java,
            newProperty(String::class.java, FAMILY_UNIX).map { dfltVal ->
                when {
                    Os.isFamily(FAMILY_WINDOWS) -> FAMILY_WINDOWS
                    Os.isFamily(FAMILY_MAC) -> FAMILY_MAC
                    Os.isFamily(FAMILY_UNIX) -> FAMILY_UNIX
                    else -> dfltVal.also {
                        LOGGER.warn("Unable to derive OS family from system properties, falling back on '$dfltVal'.")
                    }
                }
            }
        )

    /**
     * The architecture to assume for Zola process executions.
     */
    @Input
    val arch: Property<String> = newProperty(String::class.java, ARCH)

    /**
     * The version of the Zola executable to use.
     */
    @Input
    val version: Property<String> = newProperty(String::class.java, VERSION)

    /**********************************************************************/

    /**
     * URL of the 'fake' Ivy repository from which to download the executable archive.
     */
    @Input
    val ivyRepoURL: Property<String> = newProperty(String::class.java, IVY_REPO_URL)

    /**
     * Pattern layout for the 'fake' Ivy repository from which to download the executable archive.
     */
    @Input
    val ivyRepoPatternLayout: Property<String> = newProperty(String::class.java, IVY_REPO_PATTERN_LAYOUT)

    /**
     * Returns, as a String Property, the Ivy artifact identifier for the provided OS,
     * computed from the current values of the architecture and version properties.
     * @param os the OS for which to return the artifact id.
     * @return the artifact id for the requested OS.
     */
    private fun getArtifactIdPropFor(os: String): Property<String> {
        val newProp = objFact.property(String::class.java)
        val pattern = when(os) {
            FAMILY_WINDOWS -> WINDOWS_IVY_ARTIFACT_ID_PATTERN
            FAMILY_MAC -> MAC_IVY_ARTIFACT_ID_PATTERN
            FAMILY_UNIX -> LINUX_IVY_ARTIFACT_ID_PATTERN
            else -> throw Error("Implementation error at call site!")
        }
        newProp.convention(
            version.map { versionVal ->
                arch.map { archVal ->
                    MessageFormat.format(pattern, versionVal, archVal)
                }.get()
            }
        )
        return newProp
    }

    /**
     * The Ivy artifact identifier of the executable archive, if the OS is Windows.
     */
    @Input
    val windowsArtifactId: Property<String> = getArtifactIdPropFor(FAMILY_WINDOWS)

    /**
     * The Ivy artifact identifier of the executable archive, if the OS is Linux.
     */
    @Input
    val linuxArtifactId: Property<String> = getArtifactIdPropFor(FAMILY_UNIX)

    /**
     * The Ivy artifact identifier of the executable archive, if the OS is Mac.
     */
    @Input
    val macArtifactId: Property<String> = getArtifactIdPropFor(FAMILY_MAC)

    /**
     * The Ivy artifact identifier of the executable archive. Unless overwritten by the user,
     * the returned identifier depends on other properties, including: the URL and pattern layout
     * of the Ivy repository; and OS dependent artifact identifier patterns. This property is
     * supposed to be used read-only. However, in some cases, users may want to force identifier
     * of the archive containing the executable (beware that the build becomes OS dependent).
     */
    @Input
    val zolaArchiveArtifactId: Property<String> =
        newProperty(
            String::class.java,
            osFamily.map { os ->
                when(os) {
                    FAMILY_WINDOWS -> windowsArtifactId.get()
                    FAMILY_MAC -> macArtifactId.get()
                    FAMILY_UNIX -> linuxArtifactId.get()
                    else -> linuxArtifactId.get().also {
                        LOGGER.warn(
                            "Zola Plugin: falling back on UNIX for 'artifactId'." +
                                    " OS family was likely wrongly configured," +
                                    " it should be one of '$FAMILY_UNIX', '$FAMILY_WINDOWS', '$FAMILY_MAC'."
                        )
                    }
                }
            }
        )

    /**********************************************************************/

    /**
     * Prefix of the URL from which to download the Zola executable.
     */
    @Input
    val downloadURLPrefix: Property<String> = newProperty(String::class.java, DOWNLOAD_URL_PREFIX)

//    /**
//     * Internal provider to access property {@link #downloadURLPrefix} as a URI.
//     * @return the prefix of all download URLs (for every variant) as a {@link URI}.
//     */
//    // @Internal val downloadURLPrefixAsURI: Provider<URI> = downloadURLPrefix.map{ URI(it) }
//    // Crashes for an obscure reason related to modules:
//    //   "Configuration cache state could not be cached: field `downloadURLPrefixAsURI` ...
//    //     found in field `extension` of task ... module java.base does not "opens java.net" to unnamed module ...
//    fun downloadURLPrefixAsURI(): Provider<URI> = downloadURLPrefix.map{ URI(it) }

    /**
     * Returns, as a String Property, the download URL for the provided OS computed from the current
     * values of the prefix, architecture and version properties.
     * @param os the OS for which to return the download URL.
     * @return the download URL for the requested OS.
     */
    private fun getURLPropFor(os: String): Property<String> {
        val newProp = objFact.property(String::class.java)
        val pattern = when(os) {
            FAMILY_WINDOWS -> WINDOWS_DOWNLOAD_URL_PATTERN
            FAMILY_MAC -> MAC_OS_DOWNLOAD_URL_PATTERN
            FAMILY_UNIX -> LINUX_DOWNLOAD_URL_PATTERN
            else -> throw Error("Implementation error at call site!")
        }
        newProp.convention(
            downloadURLPrefix.map { uriPrefixVal ->
                arch.map { archVal ->
                    version.map { versionVal ->
                        MessageFormat.format(
                            pattern,
                            uriPrefixVal, archVal, versionVal
                        )
                    }.get()
                }.get()
            }
        )
        return newProp
    }

    /**
     * The URL from which to download the Zola executable, if the OS is Windows.
     */
    @Input
    val windowsDownloadURL: Property<String> = getURLPropFor(FAMILY_WINDOWS)

    /**
     * The URL from which to download the Zola executable, if the OS is Linux.
     */
    @Input
    val linuxDownloadURL: Property<String> = getURLPropFor(FAMILY_UNIX)

    /**
     * The URL from which to download the Zola executable, if the OS is Mac.
     */
    @Input
    val macOSDownloadURL: Property<String> = getURLPropFor(FAMILY_MAC)

    /**
     * The URL from which to download the archive containing the executable. The returned URL depends
     * on other properties, including the download URLs for the different OSs, unless overwritten by the user.
     * This property is supposed to be used read-only. However, in some case, users may want to force the URL
     * from which the archive containing the executable will be downloaded (beware that the build becomes OS
     * dependent).
     */
    @Input
    val archiveURI: Property<String> =
        newProperty(
            String::class.java,
            osFamily.map { os ->
                when(os) {
                    FAMILY_WINDOWS -> windowsDownloadURL.get()
                    FAMILY_MAC -> macOSDownloadURL.get()
                    FAMILY_UNIX -> linuxDownloadURL.get()
                    else -> linuxDownloadURL.get().also {
                        LOGGER.warn(
                            "Zola Plugin: falling back on UNIX for 'archiveURL'." +
                                    " OS family was likely wrongly configured," +
                                    " it should be one of '$FAMILY_UNIX', '$FAMILY_WINDOWS', '$FAMILY_MAC'."
                        )
                    }
                }
            }
        )

    /**********************************************************************/

    /**
     * The path to the Zola executable file in the archive
     */
    @Input
    val executablePathInArchive: Property<String> =
        newProperty(
            String::class.java,
            osFamily.map { osFamilyValue ->
                when (osFamilyValue) {
                    FAMILY_WINDOWS -> EXECUTABLE_PATH_IN_WINDOWS_ARCHIVE
                    FAMILY_MAC -> EXECUTABLE_PATH_IN_MAC_ARCHIVE
                    FAMILY_UNIX -> EXECUTABLE_PATH_IN_LINUX_ARCHIVE
                    else -> EXECUTABLE_PATH_IN_LINUX_ARCHIVE.also {
                        LOGGER.warn(
                            "Zola plugin: falling back on UNIX for 'zolaExecutablePathInArchive'." +
                                    " OS family was likely wrongly configured," +
                                    " it should be one of '$FAMILY_UNIX', '$FAMILY_WINDOWS', '$FAMILY_MAC'."
                        )
                    }
                }
            }
        )

    /**
     * A string representing a path, relative to the project root directory, to the directory where
     * to find build tools.
     */
    @Input
    val buildToolsDirPath: Property<String> = newProperty(String::class.java, BUILD_TOOLS_DIR_RELPATH)

    /**
     * The directory resulting from the interpretation of {@link #buildToolsDirPath} relative to
     * the project's root directory.
     */
    @Internal
    val buildToolsDir: Provider<Directory> = projectLayout.dir( buildToolsDirPath.map { File(it) } )

    /**
     * A string representing a path, relative to {@link #buildToolsDir}, to the Zola executable file.
     */
    @Input
    val executablePath: Property<String> =
        newProperty(
            String::class.java,
            executablePathInArchive.map{ "$EXECUTABLE_DIR_RELPATH/${File(it).name}" }
        )

    /**
     * The Zola executable {@Link RegularFile}. It corresponds to the interpretation of {@link #zolaExecutablePath}
     * relative to {@link buildToolsDir}.
     */
    @Internal
    val executableFile: Provider<RegularFile> =
        buildToolsDir.map{ rootDir ->
            executablePath.map{ path ->
                rootDir.file(path)
            }.get()
        }

    /**
     * The absolute path of the directory where to find the website sources to process with Zola.
     */
    @Input
    val sourceDirectoryPath: Property<String> = newProperty(String::class.java, SOURCE_DIRECTORY_RELPATH)

    /**
     * The directory resulting from the interpretation of {@link #sourceDirectoryPath} relative to
     * the project's root directory.
     */
    @Internal
    val sourceDirectory: Provider<Directory> =
        sourceDirectoryPath.map{ projectLayout.dir(sourceDirectoryPath.map{ File(it) }).get() }

    /**
     * The absolute path of the directory where to find the website sources to process with Zola.
     */
    @Input
    val publishDirectoryPath: Property<String> = newProperty(String::class.java, PUBLISH_DIRECTORY_RELPATH)

    /**
     * The directory resulting from the interpretation of {@link #sourceDirectoryPath} relative to
     * the project's root directory.
     */
    @Internal
    val publishDirectory: Provider<Directory> =
        publishDirectoryPath.map{ projectLayout.buildDirectory.dir(it).get() }
        // projectLayout.buildDirectory.dir( publishDirectoryPath.get() )

    /**
     * The host on which to serve the website.
     */
    @Input
    val serveOnHost: Property<String> = newProperty(String::class.java, SERVE_HOST)

    /**
     * The port on which to serve the website.
     */
    @Input
    val serveOnPort: Property<String> = newProperty(String::class.java, SERVE_PORT)

    /**
     * Whether to only rebuild the minimum on change (useful when working on a
     * specific page/section).
     */
    @Input
    val serveFast: Property<Boolean> = newProperty(Boolean::class.java, SERVE_FAST)

    /**
     * Whether to open site in the default browser.
     */
    @Input
    val autoOpenServedSite: Property<Boolean> = newProperty(Boolean::class.java, SERVE_OPEN)

    /********************************************************************/

    /**
     * Retrieve current plugin configuration as a Map of configuration option names mapped to their current
     * configuration value.
     * @return an immutable map from configuration option names to their current value.
     */
    @Internal // Required by Gradle which assumes any method with a name starting with 'get' and without parameter
              // is an Input/Output property
    fun getConfigurationOptionsAsStringsMap(): Map<String, String> {
        return mapOf(
            "osFamily" to osFamily
                .getOrElse("Error: 'osFamily' configuration option is undefined!"),
            "arch" to arch
                .getOrElse("Error: 'arch' configuration option is undefined!"),
            "version" to version
                .getOrElse("Error: 'version' configuration option is undefined!"),
            "ivyRepoURL" to ivyRepoURL
                .getOrElse("Error: 'ivyRepoURL' configuration option is undefined!"),
            "ivyRepoPatternLayout" to ivyRepoPatternLayout
                .getOrElse("Error: 'ivyRepoPatternLayout' configuration option is undefined!"),
//            "windowsArtifactId" to windowsArtifactId
//                .getOrElse("Error: 'windowsArtifactId' configuration option is undefined!"),
//            "linuxArtifactId" to linuxArtifactId
//                .getOrElse("Error: 'linuxArtifactId' configuration option is undefined!"),
//            "macArtifactId" to macArtifactId
//                .getOrElse("Error: 'macArtifactId' configuration option is undefined!"),
            "artifactId" to zolaArchiveArtifactId
                .getOrElse("Error: 'artifactId' configuration option is undefined!"),
//            "downloadURLPrefix" to
//                    downloadURLPrefix.map {
//                        "$it (${downloadURLPrefix.map{URI(it).toASCIIString()}.get()})"
//                    }.getOrElse("Error: 'downloadURLPrefix' configuration option is undefined!"),
//            "windowsDownloadUrl" to windowsDownloadURL
//                .getOrElse("Error: 'windowsDownloadUrl' configuration option is undefined!"),
//            "linuxDownloadUrl" to linuxDownloadURL
//                .getOrElse("Error: 'linuxDownloadUrl' configuration option is undefined!"),
//            "macOSDownloadUrl" to macOSDownloadURL
//                .getOrElse("Error: 'macOSDownloadUrl' configuration option is undefined!"),
            "archiveURL" to archiveURI
                .getOrElse("Error: 'archiveURL' configuration option is undefined!"),
            "zolaExecutablePathInArchive" to executablePathInArchive
                .getOrElse("Error: 'zolaExecutablePathInArchive' configuration option is undefined!"),
            "buildToolsDirPath" to
                    buildToolsDirPath.map {
                        "$it (${buildToolsDir.map{ it.asFile.absolutePath }.get()})"
                    }.getOrElse("Error: 'buildToolsDirPath' configuration option is undefined!"),
            "zolaExecutablePath" to
                    executablePath.map {
                        "$it (${executableFile.map{ it.asFile.absolutePath }.get()})"
                    }.getOrElse("Error: 'zolaExecutablePath' configuration option is undefined!"),
            "sourceDirectoryPath" to
                    sourceDirectoryPath.map {
                        "$it (${sourceDirectory.map{ it.asFile.absolutePath }.get()})"
                    }.getOrElse("Error: 'sourceDirectoryPath' configuration option is undefined!"),
            "publishDirectoryPath" to
                    publishDirectoryPath.map {
                        "$it (${publishDirectory.map{ it.asFile.absolutePath }.get()})"
                    }.getOrElse("Error: 'publishDirectoryPath' configuration option is undefined!"),
            "serveOnHost" to serveOnHost
                .getOrElse("Warning: 'serveOnHost' configuration option is undefined!"),
            "serveOnPort" to serveOnPort
                    .getOrElse("Warning: 'serveOnPort' configuration option is undefined!"),
            "serveFast" to serveFast.map(Boolean::toString)
                    .getOrElse("Warning: 'serveFast' configuration option is undefined!"),
            "autoOpenServedSite" to autoOpenServedSite.map(Boolean::toString)
                    .getOrElse("Warning: 'autoOpenServedSite' configuration option is undefined!"),
        )
    }

    /**
     * Retrieve current plugin configuration as a List of strings describing options as requested by parameter.
     * @param optionToString a function transforming option name (first parameter) and current value (second
     * parameter) to the desired string in the resulting list.
     * @return an immutable list describing options as specified by the parameter {@code optionToString}.
     */
    fun getConfigurationOptionsAsStringList(optionToString: (String, String) -> String): List<String> {
        return getConfigurationOptionsAsStringsMap().map{ e -> optionToString(e.key, e.value) }
    }

    fun downloadUrl(): URL {
        return when (osFamily.orNull) {
            FAMILY_WINDOWS -> windowsDownloadURL
            FAMILY_MAC -> macOSDownloadURL
            FAMILY_UNIX -> linuxDownloadURL
            else -> linuxDownloadURL.also {
                LOGGER.warn("Zola plugin: falling back on UNIX for 'downloadUrl'. OS family was likely wrongly configured," +
                        " it should be one of '$FAMILY_UNIX', '$FAMILY_WINDOWS', '$FAMILY_MAC'.")
            }
        }.let { URI(it.get()).toURL() }
    }
}
