package io.gitlab.mko_utilities.gradle_plugins.zola.tasks

import io.gitlab.mko_utilities.gradle_plugins.zola.*
import io.gitlab.mko_utilities.gradle_plugins.zola.PluginInfo.PLUGIN_EXTENSION_ID
import io.gitlab.mko_utilities.gradle_plugins.zola.ZolaPlugin.Companion.COMMAND_TASK_NAME
import io.gitlab.mko_utilities.gradle_plugins.zola.ZolaPlugin.Companion.INIT_TASK_NAME
import io.gitlab.mko_utilities.gradle_plugins.zola.ZolaPlugin.Companion.RETRIEVE_EXECUTABLE_TASK_NAME
import io.gitlab.mko_utilities.gradle_plugins.zola.extensions.ZolaPluginExtension
import io.gitlab.mko_utilities.gradle_plugins.zola.extensions.ZolaPluginExtension.Constants.VERSION
import org.assertj.core.api.Assertions.assertThat
import org.gradle.testkit.runner.TaskOutcome.FAILED
import org.gradle.testkit.runner.TaskOutcome.FROM_CACHE
import org.gradle.testkit.runner.TaskOutcome.SUCCESS
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import java.io.File

class ZolaSubcommandTest {

    companion object {
        const val TEST_SITE_DIRECTORY = "testSiteDirectory"
    }

    private lateinit var testProject: TestProject

    @BeforeEach
    fun `init test project`() {
        testProject = TestProject()
            .initSettingsFile()
            .initBuildFile()
    }

    @AfterEach
    fun `delete test project`() {
        testProject.getRootDirectory().deleteRecursively()
    }

    @Test
    fun `zola should run zola command provided in command option`() {
        testProject.initWithProjectTemplate()
        testProject.run(WITH_BUILD_CACHE, COMMAND_TASK_NAME, "--extra-arguments=--version").also {
            assertThat(it.task(":$RETRIEVE_EXECUTABLE_TASK_NAME")!!.outcome).isIn(SUCCESS, FROM_CACHE)
            assertThat(it.task(":$COMMAND_TASK_NAME")!!.outcome).isEqualTo(SUCCESS)
            assertThat(it.output).contains("zola $VERSION")
        }
    }

    @Test
    fun `zola should fail when command fails`() {
        testProject.initWithProjectTemplate()
        testProject.runAndFail(WITH_BUILD_CACHE, COMMAND_TASK_NAME, "--extra-arguments=invalid").also {
            assertThat(it.task(":$COMMAND_TASK_NAME")!!.outcome).isEqualTo(FAILED)
        }
    }

//    The following 2 tests can't work as Zola's init command expect user inputs
//    Error: "Error: unable to read from stdin for confirmation"

    // @Test
    fun `zola should build new zola site in base directory by default`() {
        testProject.run(WITH_BUILD_CACHE, INIT_TASK_NAME).also {
            assertThat(it.task(":$RETRIEVE_EXECUTABLE_TASK_NAME")!!.outcome).isIn(SUCCESS, FROM_CACHE)
            assertThat(it.task(":$COMMAND_TASK_NAME")!!.outcome).isEqualTo(SUCCESS)
        }
        val project = testProject.getProject()
        val dfltZolaExt =
            project.extensions.create(PLUGIN_EXTENSION_ID, ZolaPluginExtension::class.java, project)
        assertThat(File(dfltZolaExt.sourceDirectory.get().file("config.toml").asFile.absolutePath)).isFile
    }

    // @Test
    fun `zola should run zola command in requested source directory`() {
        testProject
            .initBuildFile {
                appendText("""
                       $PLUGIN_EXTENSION_ID {
                           sourceDirectory = "$TEST_SITE_DIRECTORY"
                       }
                       """.trimIndent()
                )
            }
        testProject.run(WITH_BUILD_CACHE, INIT_TASK_NAME).also {
            assertThat(it.task(":$RETRIEVE_EXECUTABLE_TASK_NAME")!!.outcome).isIn(SUCCESS, FROM_CACHE)
            assertThat(it.task(":$COMMAND_TASK_NAME")!!.outcome).isEqualTo(SUCCESS)
            assertThat(
                testProject.getRootDirectory().resolve("${TEST_SITE_DIRECTORY}/config.toml")
            ).isFile
        }
    }

}
