package io.gitlab.mko_utilities.gradle_plugins.zola.extensions

import io.gitlab.mko_utilities.gradle_plugins.zola.PluginInfo.PLUGIN_EXTENSION_ID
import io.gitlab.mko_utilities.gradle_plugins.zola.PluginInfo.PLUGIN_ID
import io.gitlab.mko_utilities.gradle_plugins.zola.PluginInfo.PLUGIN_INFO_TASK_NAME
import io.gitlab.mko_utilities.gradle_plugins.zola.PluginInfo.PLUGIN_NAME
import io.gitlab.mko_utilities.gradle_plugins.zola.PluginInfo.PLUGIN_VERSION
import io.gitlab.mko_utilities.gradle_plugins.zola.TestProject
import io.mockk.clearAllMocks
import io.mockk.every
import io.mockk.mockkStatic
import org.apache.tools.ant.taskdefs.condition.Os
import org.apache.tools.ant.taskdefs.condition.Os.FAMILY_MAC
import org.apache.tools.ant.taskdefs.condition.Os.FAMILY_UNIX
import org.apache.tools.ant.taskdefs.condition.Os.FAMILY_WINDOWS
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.gradle.testfixtures.ProjectBuilder

import org.gradle.api.Project
import org.gradle.kotlin.dsl.extra
import org.junit.jupiter.api.Assertions.assertTrue
import java.net.URI

class ZolaPluginExtensionTest {
    companion object {
        private const val ARCHIVE_PATH_PREFIX = "getzola/zola/releases/download"
        private const val TESTED_VERSION = "v0.18.0"
        private const val TESTED_ARCH = "x86_64"
        private const val VERSIONED_ARCHIVE_PATH_PREFIX =
            "$ARCHIVE_PATH_PREFIX/$TESTED_VERSION/zola-$TESTED_VERSION-$TESTED_ARCH"
        const val WINDOWS_ARCHIVE = "$VERSIONED_ARCHIVE_PATH_PREFIX-pc-windows-msvc.zip"
        const val LINUX_ARCHIVE = "$VERSIONED_ARCHIVE_PATH_PREFIX-unknown-linux-gnu.tar.gz"
        const val MAC_OS_ARCHIVE = "$VERSIONED_ARCHIVE_PATH_PREFIX-apple-darwin.tar.gz"
    }

    private lateinit var testProject: TestProject

    @BeforeEach fun `init mocks`() { mockkStatic(Os::class) }

    @BeforeEach
    fun `init test project`() {
        testProject = TestProject().initSettingsFile().initBuildFile()
    }

    @AfterEach
    fun `delete test project`() {
        testProject.getRootDirectory().deleteRecursively()
    }

    @AfterEach fun `clear mocks`() { clearAllMocks() }

    @Test
    fun `downloadUrl should return download URL for Windows Zola binary when OS family is Windows`() {
        every { Os.isFamily(any()) }.returns(false)
        every { Os.isFamily(FAMILY_WINDOWS) }.returns(true)
        val project = testProject.getProject()
        val ext = project.extensions.create(PLUGIN_EXTENSION_ID, ZolaPluginExtension::class.java, project)
        assertThat(URI(ext.archiveURI.get()).toURL()).hasPath("/$WINDOWS_ARCHIVE")
    }

    @Test
    fun `downloadUrl should return download URL for Linux Zola binary when OS family is Unix`() {
        every { Os.isFamily(any()) }.returns(false)
        every { Os.isFamily(FAMILY_UNIX) }.returns(true)
        val project = testProject.getProject()
        val ext = project.extensions.create(PLUGIN_EXTENSION_ID, ZolaPluginExtension::class.java, project)
        assertThat(URI(ext.archiveURI.get()).toURL()).hasPath("/$LINUX_ARCHIVE")
    }

    @Test
    fun `downloadUrl should return download URL for macOS Zola binary when OS family is macOS`() {
        every { Os.isFamily(any()) }.returns(false)
        every { Os.isFamily(FAMILY_MAC) }.returns(true)
        val project = testProject.getProject()
        val ext = project.extensions.create(PLUGIN_EXTENSION_ID, ZolaPluginExtension::class.java, project)
        assertThat(URI(ext.archiveURI.get()).toURL()).hasPath("/$MAC_OS_ARCHIVE")
    }

    @Test
    fun `downloadUrl should return download URL for linux Zola binary when OS family can't be derived from system property`() {
        every { Os.isFamily(any()) }.returns(false)
        val project = testProject.getProject()
        val ext = project.extensions.create(PLUGIN_EXTENSION_ID, ZolaPluginExtension::class.java, project)
        assertThat(URI(ext.archiveURI.get()).toURL()).hasPath("/$LINUX_ARCHIVE")
    }

    @Test
    fun `can parse configuration DSL`() {
        testProject.initBuildFile {
            appendText("""
                $PLUGIN_EXTENSION_ID {
                  osFamily = "unix"
                  arch = "x86_64"
                  version = "0.18.0"
                  downloadURLPrefix = "https://github.com/getzola/zola/releases/download"
                  windowsDownloadURL = "https://github.com/getzola/zola/releases/download/windows.zip"
                  linuxDownloadURL = "https://github.com/getzola/zola/releases/download/linux.tgz"
                  macOSDownloadURL = "https://github.com/getzola/zola/releases/download/mac.tar.gz"
                  buildToolsDirPath = "buildTools"
                  executablePath = "zola/bin/zola"
                  sourceDirectoryPath = "site"
                  publishDirectoryPath = "zola/publish"
                }
                """.trimIndent()
            )
        }
        val test = testProject.run(PLUGIN_INFO_TASK_NAME)

        // Verify the result
        assertTrue(test.output.contains("This is plugin '$PLUGIN_NAME'"))
        assertTrue(test.output.contains("v$PLUGIN_VERSION"))
    }

    @Test
    fun `take into account user-defined osFamily`() {
        testProject.initBuildFile {
            appendText("""
                $PLUGIN_EXTENSION_ID {
                    osFamily = "windows"
                }
                """.trimIndent()
            )
        }
        testProject.run(PLUGIN_INFO_TASK_NAME)
    }

    @Test
    fun `take into account user-defined version`() {
        testProject.initBuildFile {
            appendText("""
                $PLUGIN_EXTENSION_ID {
                    version = "0.10.0"
                }
                """.trimIndent()
            )
        }
        testProject.run(PLUGIN_INFO_TASK_NAME)
    }

    @Test
    fun `take into account user-defined URL prefix`() {
        testProject.initBuildFile {
            appendText("""
                $PLUGIN_EXTENSION_ID {
                    downloadURLPrefix = "https://github.com/getzola2/zola/releases/download"
                }
                """.trimIndent()
            )
        }
        testProject.run(PLUGIN_INFO_TASK_NAME)
    }

    @Test
    fun `take into account user-defined buildToolsDirPath`() {
        testProject.initBuildFile {
            appendText("""
                $PLUGIN_EXTENSION_ID {
                    buildToolsDirPath = "build/tools"
                }
                """.trimIndent()
            )
        }
        testProject.run(PLUGIN_INFO_TASK_NAME)
    }

    @Test
    fun `take into account user-defined absolute buildToolsDirPath`() {
        testProject.initBuildFile {
            appendText("""
                $PLUGIN_EXTENSION_ID {
                    buildToolsDirPath = "/tmp/zola/build-tools"
                }
                """.trimIndent()
            )
        }
        testProject.run(PLUGIN_INFO_TASK_NAME)
    }

    @Test
    fun `take into account user-defined buildToolsDirPath in construction of default zolaExecutablePath`() {
        testProject.initBuildFile {
            appendText("""
                $PLUGIN_EXTENSION_ID {
                    buildToolsDirPath = "build/tools"
                    executablePathInArchive = "zola.test"
                }
                """.trimIndent()
            )
        }
        val test = testProject.run(PLUGIN_INFO_TASK_NAME)
        val project = testProject.getProject()
    }
}
