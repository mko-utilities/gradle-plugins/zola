package io.gitlab.mko_utilities.gradle_plugins.zola

import org.junit.jupiter.api.DisplayNameGenerator
import java.lang.reflect.Method

/**
 * JUnit test name generator that appends the tested Gradle version to all test names.
 */
class GradleVersionDisplayNameGenerator : DisplayNameGenerator.Standard() {
    private val dnSuffix = displayNameSuffix()
    override fun generateDisplayNameForMethod(testClass: Class<*>, testMethod: Method): String {
        return "${testMethod.name}$dnSuffix"
    }
}
