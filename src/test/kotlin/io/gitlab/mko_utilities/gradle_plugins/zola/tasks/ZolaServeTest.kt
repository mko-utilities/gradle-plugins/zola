package io.gitlab.mko_utilities.gradle_plugins.zola.tasks

import io.gitlab.mko_utilities.gradle_plugins.zola.TestProject
import io.gitlab.mko_utilities.gradle_plugins.zola.WITH_BUILD_CACHE
import io.gitlab.mko_utilities.gradle_plugins.zola.ZolaPlugin.Companion.RETRIEVE_EXECUTABLE_TASK_NAME
import io.gitlab.mko_utilities.gradle_plugins.zola.ZolaPlugin.Companion.SERVE_TASK_NAME
import io.gitlab.mko_utilities.gradle_plugins.zola.extensions.ZolaPluginExtension.Constants.PUBLISH_DIRECTORY_RELPATH
import io.gitlab.mko_utilities.gradle_plugins.zola.extensions.ZolaPluginExtension.Constants.SOURCE_DIRECTORY_RELPATH
import org.assertj.core.api.Assertions.assertThat
import org.gradle.testkit.runner.TaskOutcome.FROM_CACHE
import org.gradle.testkit.runner.TaskOutcome.SUCCESS
import org.gradle.testkit.runner.TaskOutcome.FAILED
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import java.io.File

class ZolaServeTest {

    private lateinit var testProject: TestProject

    @BeforeEach
    fun `init test project`() {
        testProject = TestProject()
            .initSettingsFile()
            .initBuildFile()
    }

    @AfterEach
    fun `delete test project`() {
        testProject.getRootDirectory().deleteRecursively()
    }

    private fun projectDir(relPath: String): File {
        return testProject.getProject().layout.projectDirectory.dir(relPath).asFile
    }

    private fun sourceFile(extraPath: String = "."): File {
        return File(projectDir(SOURCE_DIRECTORY_RELPATH), extraPath)
    }

    private fun builtFile(extraPath: String = "."): File {
        return File(projectDir("build"), extraPath)
    }

    private fun publishedFile(extraPath: String = "."): File {
        return File(builtFile(PUBLISH_DIRECTORY_RELPATH), extraPath)
    }

    @Test
    fun `zola should serve zola site in default source directory with default parameters`() {
        testProject.initWithProjectTemplate()
        testProject.runAndFail(WITH_BUILD_CACHE, SERVE_TASK_NAME, "--extra-arguments=--forceFailure").also {
            assertThat(it.task(":$RETRIEVE_EXECUTABLE_TASK_NAME")!!.outcome).isIn(SUCCESS, FROM_CACHE)
            assertThat(it.task(":$SERVE_TASK_NAME")!!.outcome).isEqualTo(FAILED)
            assertThat(it.output).contains("error: unexpected argument '--forceFailure' found")
        }
    }

    @Test
    fun `zola should serve zola site in source directory on requested baseUrl`() {
        testProject.initWithProjectTemplate()
        testProject.runAndFail(WITH_BUILD_CACHE, SERVE_TASK_NAME, "--base-url=http://localhost:1313/documentation", "--extra-arguments=--forceFailure").also {
            assertThat(it.task(":$RETRIEVE_EXECUTABLE_TASK_NAME")!!.outcome).isIn(SUCCESS, FROM_CACHE)
            assertThat(it.task(":$SERVE_TASK_NAME")!!.outcome).isEqualTo(FAILED)
            assertThat(it.output).contains("error: unexpected argument '--forceFailure' found")
        }
    }

    @Test
    fun `zola should fail if source directory does not exists`() {
        testProject.initWithProjectTemplate()
        testProject.initBuildFile {
            appendText("""
                zola {
                  sourceDirectoryPath = "other"
                }
            """.trimIndent())
        }
        testProject.runAndFail(WITH_BUILD_CACHE, SERVE_TASK_NAME).also {
            assertThat(it.task(":$RETRIEVE_EXECUTABLE_TASK_NAME")!!.outcome).isIn(SUCCESS, FROM_CACHE)
            assertThat(it.task(":$SERVE_TASK_NAME")!!.outcome).isEqualTo(FAILED)
            assertThat(it.output).contains("Reason: An input file was expected to be present but it doesn't exist.")
        }
    }

    @Test
    fun `zola should serve zola site in requested source directory`() {
        testProject.initWithProjectTemplate()
        testProject.initBuildFile {
            appendText("""
                zola {
                  sourceDirectoryPath = "other"
                }
            """.trimIndent())
        }
        projectDir("other").mkdirs()
        testProject.runAndFail(WITH_BUILD_CACHE, SERVE_TASK_NAME).also {
            assertThat(it.task(":$RETRIEVE_EXECUTABLE_TASK_NAME")!!.outcome).isIn(SUCCESS, FROM_CACHE)
            assertThat(it.task(":$SERVE_TASK_NAME")!!.outcome).isEqualTo(FAILED)
            assertThat(it.output).contains("Error: config.toml not found in current directory or ancestors")
        }
    }

    @Test
    fun `zola should serve zola site in source directory with additional parameters`() {
        testProject.initWithProjectTemplate()
        testProject.runAndFail(WITH_BUILD_CACHE, SERVE_TASK_NAME, "--extra-arguments=--port 1314 --forceFailure").also {
            assertThat(it.task(":$RETRIEVE_EXECUTABLE_TASK_NAME")!!.outcome).isIn(SUCCESS, FROM_CACHE)
            assertThat(it.task(":$SERVE_TASK_NAME")!!.outcome).isEqualTo(FAILED)
            assertThat(it.output).contains("error: unexpected argument '--forceFailure' found")
        }
    }
}
