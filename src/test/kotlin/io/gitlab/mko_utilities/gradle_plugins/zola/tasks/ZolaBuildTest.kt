package io.gitlab.mko_utilities.gradle_plugins.zola.tasks

import io.gitlab.mko_utilities.gradle_plugins.zola.*
import io.gitlab.mko_utilities.gradle_plugins.zola.ZolaPlugin.Companion.BUILD_TASK_NAME
import io.gitlab.mko_utilities.gradle_plugins.zola.ZolaPlugin.Companion.RETRIEVE_EXECUTABLE_TASK_NAME
import io.gitlab.mko_utilities.gradle_plugins.zola.extensions.ZolaPluginExtension.Constants.PUBLISH_DIRECTORY_RELPATH
import io.gitlab.mko_utilities.gradle_plugins.zola.extensions.ZolaPluginExtension.Constants.SOURCE_DIRECTORY_RELPATH
import org.assertj.core.api.Assertions.assertThat
import org.gradle.api.file.Directory
import org.gradle.testkit.runner.TaskOutcome.FAILED
import org.gradle.testkit.runner.TaskOutcome.FROM_CACHE
import org.gradle.testkit.runner.TaskOutcome.SUCCESS
import org.jetbrains.kotlin.gradle.internal.ensureParentDirsCreated
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import java.io.File

class ZolaBuildTest {

    private lateinit var testProject: TestProject

    @BeforeEach
    fun `init test project`() {
        testProject = TestProject()
            .initSettingsFile()
            .initBuildFile()
    }

    @AfterEach
    fun `delete test project`() {
        testProject.getRootDirectory().deleteRecursively()
    }

    private fun projectDir(relPath: String): File {
        return testProject.getProject().layout.projectDirectory.dir(relPath).asFile
    }

    private fun sourceFile(extraPath: String = "."): File {
        return File(projectDir(SOURCE_DIRECTORY_RELPATH), extraPath)
    }

    private fun builtFile(extraPath: String = "."): File {
        return File(projectDir("build"), extraPath)
    }

    private fun publishedFile(extraPath: String = "."): File {
        return File(builtFile(PUBLISH_DIRECTORY_RELPATH), extraPath)
    }

    @Test
    fun `zolaBuild should build new zola site in output directory`() {
        testProject.initWithProjectTemplate()
        testProject.run(WITH_BUILD_CACHE, BUILD_TASK_NAME).also {
            assertThat(it.task(":$RETRIEVE_EXECUTABLE_TASK_NAME")!!.outcome).isIn(SUCCESS, FROM_CACHE)
            assertThat(it.task(":$BUILD_TASK_NAME")!!.outcome).isEqualTo(SUCCESS)
            assertThat(publishedFile("index.html")).isFile
            assertThat(publishedFile("draft/index.html")).doesNotExist()
        }
    }

    @Test
    fun `zolaBuild should build from requested source directory`() {
        testProject.initBuildFile {
            appendText("""
                zola {
                  sourceDirectoryPath = "site"
                }
            """.trimIndent())
        }
        testProject.initWithProjectTemplate(dstDir = projectDir("site"))
        testProject.run(WITH_BUILD_CACHE, BUILD_TASK_NAME).also {
            assertThat(it.task(":$RETRIEVE_EXECUTABLE_TASK_NAME")!!.outcome).isIn(SUCCESS, FROM_CACHE)
            assertThat(it.task(":$BUILD_TASK_NAME")!!.outcome).isEqualTo(SUCCESS)
            assertThat(publishedFile("index.html")).isFile
            assertThat(publishedFile("draft/index.html")).doesNotExist()
        }
    }

    @Test
    fun `zolaBuild should fail when site generation fails`() {
        testProject.initWithProjectTemplate()
        sourceFile("config.toml").delete()
        testProject.runAndFail(WITH_BUILD_CACHE, BUILD_TASK_NAME).also {
            assertThat(it.task(":$BUILD_TASK_NAME")!!.outcome).isEqualTo(FAILED)
            assertThat(File(publishedFile(), "index.html")).doesNotExist()
        }
    }

    @Test
    fun `zolaBuild should build new zola site in requested relative path output directory provided in plugin configuration`() {
        testProject.initWithProjectTemplate()
        testProject.initBuildFile {
            appendText("""
                zola {
                  publishDirectoryPath = "testOutput"
                }
            """.trimIndent())
        }
        testProject.run(WITH_BUILD_CACHE, BUILD_TASK_NAME).also {
            assertThat(it.task(":$BUILD_TASK_NAME")!!.outcome).isEqualTo(SUCCESS)
            assertThat(builtFile("testOutput/index.html")).isFile
        }
    }

    @Test
    fun `zolaBuild should build new zola site in requested absolute path output directory provided in plugin configuration`() {
        testProject.initWithProjectTemplate()
        testProject.initBuildFile {
            appendText("""
                zola {
                  // publishDirectoryPath = File(layout.projectDirectory.asFile, "testOutput").absolutePath
                  publishDirectoryPath = layout.projectDirectory.dir("testOutput").asFile.absolutePath
                }
            """.trimIndent())
        }
        testProject.run(WITH_BUILD_CACHE, BUILD_TASK_NAME).also {
            assertThat(it.task(":$BUILD_TASK_NAME")!!.outcome).isEqualTo(SUCCESS)
            assertThat(projectDir("testOutput/index.html")).isFile
        }
    }

    @Test
    fun `zolaBuild should build new zola site in requested absolute path output directory provided in task configuration`() {
        testProject.initWithProjectTemplate()
        testProject.initBuildFile {
            appendText("""
                tasks.withType<${ZolaBuildTask::class.java.name}> {
                  // outputDirectory = File(layout.buildDirectory.get().asFile, "testOutput")
                  outputDirectory = layout.buildDirectory.dir("testOutput")
                }
            """.trimIndent())
        }
        testProject.run(WITH_BUILD_CACHE, BUILD_TASK_NAME).also {
            assertThat(it.task(":$BUILD_TASK_NAME")!!.outcome).isEqualTo(SUCCESS)
            assertThat(builtFile("testOutput/index.html")).isFile
        }
    }

    @Test
    fun `zolaBuild should cleanup output directory`() {
        val outdatedFile = publishedFile("outdated.html")
        testProject.initWithProjectTemplate()
        outdatedFile.apply {
            ensureParentDirsCreated()
            createNewFile()
        }
        assertThat(outdatedFile).isFile()
        testProject.run(WITH_BUILD_CACHE, BUILD_TASK_NAME).also {
            assertThat(it.task(":$BUILD_TASK_NAME")!!.outcome).isEqualTo(SUCCESS)
            assertThat(outdatedFile).doesNotExist()
        }
    }

    // @TODO: Fix this test. It does not retrieve result from CACHE ... but still SUCCESSful
    // @Test
    fun `zolaBuild should retrieve output from cache when it was already executed`() {
        testProject.initWithProjectTemplate()
        testProject.run(WITH_BUILD_CACHE, BUILD_TASK_NAME).also {
            assertThat(it.task(":$BUILD_TASK_NAME")!!.outcome).isEqualTo(SUCCESS)
        }
        publishedFile().deleteRecursively()
        testProject.run(WITH_BUILD_CACHE, BUILD_TASK_NAME).also {
            assertThat(it.task(":$BUILD_TASK_NAME")!!.outcome).isEqualTo(FROM_CACHE)
            assertThat(publishedFile("index.html")).isFile
        }
    }

    @Test
    fun `zolaBuild should take build parameters into account`() {
        testProject.initWithProjectTemplate()
        testProject.run(WITH_BUILD_CACHE, BUILD_TASK_NAME, "--drafts").also {
            assertThat(it.task(":$RETRIEVE_EXECUTABLE_TASK_NAME")!!.outcome).isIn(SUCCESS, FROM_CACHE)
            assertThat(it.task(":$BUILD_TASK_NAME")!!.outcome).isEqualTo(SUCCESS)
            assertThat(publishedFile("index.html")).isFile
            assertThat(publishedFile("draft/index.html")).isFile
        }
    }

    @Test
    fun `zolaBuild should take extra arguments into account`() {
        testProject.initWithProjectTemplate()
        testProject.run(WITH_BUILD_CACHE, BUILD_TASK_NAME, "--extra-arguments=--drafts").also {
            assertThat(it.task(":$RETRIEVE_EXECUTABLE_TASK_NAME")!!.outcome).isIn(SUCCESS, FROM_CACHE)
            assertThat(it.task(":$BUILD_TASK_NAME")!!.outcome).isEqualTo(SUCCESS)
            assertThat(publishedFile("index.html")).isFile
            assertThat(publishedFile("draft/index.html")).isFile
        }
    }
}
